import requests
import json

if __name__ == '__main__':
    url = "https://language.googleapis.com/v1/documents:analyzeEntities"
    headers = {
        'X-Goog-Api-Key': '$GOOGLE_API_KEY',
        'Content-Type': 'application/json',
    }
    data = {
        "document": {
            "content": "The rain in Spain stays mainly in the plain.",
            "type": "PLAIN_TEXT"
        }
    }
    r = requests.post(url=url, headers=headers, data=json.dumps(data))
    print(r.status_code, r.reason)
    print(r.text)

